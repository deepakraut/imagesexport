package com.softcell.imageexporter.dao;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.softcell.imageexporter.model.collection.DateOfExportedImages;
import com.softcell.imageexporter.model.collection.ExportedImagesLog;
import com.softcell.imageexporter.model.collection.gonogo.ImagesDetails;
import com.softcell.imageexporter.model.collection.gonogo.LOSDetails;
import com.softcell.imageexporter.model.collection.gonogo.ThreadLossBreDetails;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mahesh on 17/1/18.
 */
@Repository
public class ImagesExporterMongoRepository implements ImagesExporterRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private GridFsTemplate gridFsTemplate;

    private static final Logger logger = LoggerFactory.getLogger(ImagesExporterMongoRepository.class);

    @Override
    public Date getDateOfPreviousExportedImages(String institutionId) {

        Query query = new Query();
        query.addCriteria(Criteria.where("active").is(true).and("institutionId").is(institutionId));
        DateOfExportedImages dateOfExportedImages = mongoTemplate.findOne(query, DateOfExportedImages.class);

        Date previousExportedDate = null;

        if (null != dateOfExportedImages) {

            List<Date> listOfExportedImagesDate = dateOfExportedImages.getListOfExportedImagesDate();

            if (!CollectionUtils.isEmpty(listOfExportedImagesDate)) {

                //get last date of list
                previousExportedDate = listOfExportedImagesDate.get(listOfExportedImagesDate.size() - 1);
            } else {
                //get start date of application
                previousExportedDate = dateOfExportedImages.getStartDate();
            }
        }

        return previousExportedDate;
    }

    @Override
    public List<String> getApplicationIdsAgainstParticularDate(Date zeroTimeDate, Date endTimeToDate, String institutionId) {

        List<String> referenceIds = null;

      /*  Set<String> stages = new HashSet<String>();
        stages.add(GNGWorkflowConstant.DO.toFaceValue());
        stages.add(GNGWorkflowConstant.LOS_QDE.toFaceValue());
        stages.add(GNGWorkflowConstant.LOS_APRV.toFaceValue());
        stages.add(GNGWorkflowConstant.LOS_DISB.toFaceValue());
        stages.add(GNGWorkflowConstant.LOS_BDE.toFaceValue());*/
        try {
           // Query query = new Query();

            /*query.addCriteria(Criteria.where("applicationRequest.header.institutionId").is(institutionId)
                    .and("applicationRequest.header.product").in(GNGWorkflowConstant.CDL.toFaceValue(), GNGWorkflowConstant.DPL.toFaceValue())
                    .and("applicationRequest.header.dateTime").gte(zeroTimeDate).lte(endTimeToDate)
                    .and("losDetails").exists(true)
                    .and("losDetails.losID").nin(null, "", " "));

            logger.debug("Query formed get referenceIds against the date is {}", query.toString());

            referenceIds = mongoTemplate.getDb().getCollection("goNoGoCustomerApplication")
                    .distinct("_id", query.getQueryObject());*/



            Query query1 = new Query();
            query1.addCriteria(Criteria.where("stage").is("LOS_BDE")
                    .and("institutionId").is(institutionId)
                    .and("actionDate").gte(zeroTimeDate).lte(endTimeToDate));

            referenceIds = mongoTemplate.getDb().getCollection("activityLogs").distinct("refId",query1.getQueryObject());


        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception occur at the time getting application ids against date {}" + endTimeToDate);
        }

        return referenceIds;

    }


    @Override
    public List<ImagesDetails> getApplicationImagesForExport(String referenceId, String institutionID) {
        // logger.debug("getDocumentsForDmsPush repo started");
        System.out.println("satrted with" + referenceId);

        List<ImagesDetails> imageList = null;
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("metadata.gonogoReferanceId")
                    .is(referenceId).and("metadata.fileHeader.institutionId").is(institutionID)
                   // .and("metadata.uploadFileDetails.FileName").in("INVOICE_IMAGE", "APPLICANT-PHOTO")
                    .and("metadata.uploadFileDetails.deleted").is(false));

            List<GridFSDBFile> gridFSDBFileList = gridFsTemplate.find(query);

            imageList = new ArrayList<>();

            for (GridFSDBFile gridFSDBFile : gridFSDBFileList) {

                ImagesDetails imageDetail = new ImagesDetails();

                DBObject dbObject = gridFSDBFile.getMetaData();

                DBObject uploadFileDetails = (DBObject) dbObject
                        .get("uploadFileDetails");

                String imageName = null;
                String imageType = null;
                if (null != uploadFileDetails) {
                    imageName = String.valueOf(uploadFileDetails.get("FileName"));
                    imageType = String.valueOf(uploadFileDetails.get("fileType"));
                }

                boolean duplicateImage = false;

                if (StringUtils.isNotBlank(imageType) && StringUtils.isNotBlank(imageName)) {

                    for (ImagesDetails imageDetail1 : imageList) {
                        if (StringUtils.equalsIgnoreCase(imageDetail1.getImageName(), imageName)
                                && StringUtils.equalsIgnoreCase(imageDetail1.getImageType(), imageType)) {

                            duplicateImage = true;
                            break;

                        }
                    }
                    if (!duplicateImage) {
                        imageDetail.setImageName(imageName);
                        imageDetail.setImageType(imageType);
                        imageDetail.setRefID(String.valueOf(dbObject.get("gonogoReferanceId")));
                        imageDetail.setImageId(String.valueOf(gridFSDBFile.getId()));
                        imageDetail.setStatus(String.valueOf(uploadFileDetails.get("status")));
                        imageDetail.setReason(String.valueOf(uploadFileDetails.get("reason")));
                        // add in list
                        imageList.add(imageDetail);
                    }

                }
            }
            System.out.println("images size" + imageList.size());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageList;
    }

    @Override
    public GridFSDBFile getByteCodeOfImage(String imageId, String institutionId) {
        GridFSDBFile gridFSDBFile = null;
        try {
            gridFSDBFile = gridFsTemplate.findOne(new Query(Criteria.where("_id")
                    .is(imageId).and("metadata.fileHeader.institutionId")
                    .is(institutionId)));
        } catch (Exception e) {
            e.printStackTrace();

        }

        return gridFSDBFile;
    }

    @Override
    public void saveExportedImagesLog(ExportedImagesLog exportedImagesLog) {
        try {
            mongoTemplate.insert(exportedImagesLog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateDateOfExportedImages(Date endTimeToDate, String institutionId) {

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("active").is(true).and("institutionId").is(institutionId));

            Update update = new Update();
            update.push("listOfExportedImagesDate", endTimeToDate);

            mongoTemplate.updateFirst(query, update, DateOfExportedImages.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public DateOfExportedImages getExportedApplicationDate(String institutionId) {
        DateOfExportedImages dateOfExportedImages = null;
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("active").is(true).and("institutionId").is(institutionId));

            dateOfExportedImages = mongoTemplate.findOne(query, DateOfExportedImages.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateOfExportedImages;
    }

    @Override
    public List<ExportedImagesLog> getExportedImagesAgainstDate(Date fromDate, Date toDate ,String institutionId) {

        List<ExportedImagesLog> exportedImagesLogs=null;
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("exportedDate").lte(toDate).gte(fromDate));

            exportedImagesLogs = mongoTemplate.find(query, ExportedImagesLog.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return exportedImagesLogs;

    }


    @Override
    public LOSDetails getLosDetails(String institutionId, String referenceId) {

        LOSDetails losDetails = null;
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("_id").is(referenceId)
                    .and("applicationRequest.header.institutionId").is(institutionId));

            BasicDBObject fields = new BasicDBObject("losDetails", 1);


            DBCursor goNoGoCustomerApplication = mongoTemplate.getDb().getCollection("goNoGoCustomerApplication")
                    .find(query.getQueryObject(), fields);
            while (goNoGoCustomerApplication.hasNext()) {
                DBObject losDetailsObject = goNoGoCustomerApplication.next();

                if (null != losDetailsObject) {
                    DBObject losDetails1 = (DBObject) losDetailsObject.get("losDetails");
                    if (null != losDetails1) {

                        losDetails = new LOSDetails();
                        losDetails.setLosID(String.valueOf(losDetails1.get("losID")));

                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return losDetails;


    }


    @Override
    public DateOfExportedImages getSchedulerToFromDate(String institutionId) {

        Query query = new Query();

        query.addCriteria(Criteria.where("active").is(true).and("institutionId").is(institutionId));

        return mongoTemplate.findOne(query, DateOfExportedImages.class);
    }

    @Override
    public void getThreadLossApplication(String policyType, String scheme_details_bre,String newBreType) {

        try {
            Query query = new Query();



            query.addCriteria(Criteria.where("multiBREType").is(scheme_details_bre)
                    .and("scoringResponse.derivedFields.POLICY_NAME").is(policyType));


            long multiBRELogs1=0;
            do {
                multiBRELogs1 = mongoTemplate.getDb().getCollection("multiBRELogs")
                        .count(query.getQueryObject());

                System.out.println("multibreCount is " + multiBRELogs1);

                DBCursor multiBRELogs = mongoTemplate.getDb().getCollection("multiBRELogs")
                        .find(query.getQueryObject()).limit(1000);

              /*  if(null !=multiBRELogs) {
                    System.out.println("processing for records :" + multiBRELogs.count());
                }*/

                System.out.println("processing for records :" + multiBRELogs.count());
                Thread.sleep(2000);
                while (multiBRELogs.hasNext()) {

                    DBObject multiBreLog = multiBRELogs.next();

                    if (null != multiBreLog) {

                        DBObject scoringResponse = (DBObject) multiBreLog.get("scoringResponse");
                        if (null != scoringResponse) {

                            DBObject header = (DBObject) scoringResponse.get("header");

                            if (null != header) {
                                String newRefId = String.valueOf(header.get("applicationId"));
                                String oldRefId = String.valueOf(multiBreLog.get("refId"));

                                String oldbreType = String.valueOf(multiBreLog.get("multiBREType"));
                                if (StringUtils.isNotBlank(newRefId)) {

                                    multiBreLog.put("refId", newRefId);
                                    multiBreLog.put("multiBREType", newBreType);

                                    System.out.println("updating multibre data");
                                    mongoTemplate.save(multiBreLog, "multiBRELogs");

                                    System.out.println("multibre data updated successfully");

                                    ThreadLossBreDetails threadLossBreDetails = new ThreadLossBreDetails();
                                    threadLossBreDetails.setNewRefId(newRefId);
                                    threadLossBreDetails.setOldRefId(oldRefId);
                                    threadLossBreDetails.setNewBreType(newBreType);
                                    threadLossBreDetails.setOldBreType(oldbreType);
                                    threadLossBreDetails.setDateTime(new Date());

                                    System.out.println("saving log data :" + threadLossBreDetails.toString());

                                    mongoTemplate.insert(threadLossBreDetails);


                                }
                            }

                        }

                    }

                }
            } while (multiBRELogs1 != 0);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
