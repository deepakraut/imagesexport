package com.softcell.imageexporter.dao;

import com.mongodb.gridfs.GridFSDBFile;
import com.softcell.imageexporter.model.collection.DateOfExportedImages;
import com.softcell.imageexporter.model.collection.ExportedImagesLog;
import com.softcell.imageexporter.model.collection.gonogo.ImagesDetails;
import com.softcell.imageexporter.model.collection.gonogo.LOSDetails;

import java.util.Date;
import java.util.List;

/**
 * Created by mahesh on 17/1/18.
 */
public interface ImagesExporterRepository {
    /**
     *
     * @return
     */
    public Date getDateOfPreviousExportedImages(String institutionId);

    /**
     *
     * @param zeroTimeDate
     * @param endTimeToDate
     * @return
     */
    List<String> getApplicationIdsAgainstParticularDate(Date zeroTimeDate, Date endTimeToDate ,String institutionId);

    /**
     *
     * @param referenceId
     * @return
     */
    List<ImagesDetails> getApplicationImagesForExport(String referenceId,String institutionID);

    /**
     *
     * @param imageId
     * @return
     */
    GridFSDBFile getByteCodeOfImage(String imageId, String institutionId);

    /**
     *
     * @param exportedImagesLog
     */
    void saveExportedImagesLog(ExportedImagesLog exportedImagesLog);

    /**
     *
     * @param endTimeToDate
     */
    void updateDateOfExportedImages(Date endTimeToDate ,String institutionId);

    /**
     *
     * @param institutionId
     * @return
     */
    DateOfExportedImages getExportedApplicationDate(String institutionId);

    /**
     *
     * @param fromDate
     * @param toDate
     * @return
     */
    List<ExportedImagesLog> getExportedImagesAgainstDate(Date fromDate, Date toDate ,String institutionId);

    /**
     *
     * @param institutionId
     * @param referenceId
     * @return
     */
    LOSDetails getLosDetails(String institutionId, String referenceId);

    /**
     *
     * @param institutionId
     * @return
     */
    DateOfExportedImages getSchedulerToFromDate(String institutionId);

    void getThreadLossApplication(String s, String scheme_details_bre ,String newBreType);
}
