package com.softcell.imageexporter.service;

import com.mongodb.gridfs.GridFSDBFile;
import com.softcell.imageexporter.dao.ImagesExporterRepository;
import com.softcell.imageexporter.model.collection.DateOfExportedImages;
import com.softcell.imageexporter.model.collection.ExportedImagesLog;
import com.softcell.imageexporter.model.collection.gonogo.Document;
import com.softcell.imageexporter.model.collection.gonogo.ImagesDetails;
import com.softcell.imageexporter.model.collection.gonogo.LOSDetails;
import com.softcell.imageexporter.utils.DateUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mahesh on 19/1/18.
 */
@Service
public class ImageExporterManagerImpl implements ImageExporterManager {

    @Autowired
    private ImagesExporterRepository imagesExporterRepository;


    @Override
    public List<Document> getDocumentsToZip(String institutionId, String referenceId) {

        List<ImagesDetails> applicationImagesForExport = imagesExporterRepository.getApplicationImagesForExport(referenceId, institutionId);


        List<Document> documents = new ArrayList<>();
        Document doc = null;
        if(!CollectionUtils.isEmpty(applicationImagesForExport)) {

       //   LOSDetails losDetails = imagesExporterRepository.getLosDetails(institutionId, referenceId);

            for (ImagesDetails imagesDetails : applicationImagesForExport) {

                try {
                    GridFSDBFile gridFSDBFile = imagesExporterRepository.getByteCodeOfImage(imagesDetails.getImageId(), institutionId);

                    if (null != gridFSDBFile && null != gridFSDBFile.getInputStream()) {

                        doc = new Document();
                      /*  if (null != losDetails) {
                            doc.setLosID(losDetails.getLosID());
                        }*/

                        doc.setDocID(imagesDetails.getImageId());
                        doc.setByteCode(IOUtils.toByteArray(gridFSDBFile.getInputStream()));
                        String extension=imagesDetails.getImageType();
                        if("application/pdf".equalsIgnoreCase(imagesDetails.getImageType())){
                            extension="pdf";
                        }else if("jpg".equalsIgnoreCase(imagesDetails.getImageType())){
                            extension="png";
                        }
                        doc.setDocName(imagesDetails.getImageName()+"."+extension);
                        documents.add(doc);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return documents;
    }

    @Override
    public DateOfExportedImages getExportedApplicationDate(String institutionId) {
        return imagesExporterRepository.getExportedApplicationDate(institutionId);
    }

    @Override
    public List<ExportedImagesLog> getExportedImagesAgainstDate(Date date,String institutionId) {
        Date fromDate = DateUtils.getZeroTimeFromDate(date);
        Date toDate = DateUtils.getEndTimeToDate(date);

        return imagesExporterRepository.getExportedImagesAgainstDate(fromDate, toDate ,institutionId);
    }
}
