package com.softcell.imageexporter.service;

import com.softcell.imageexporter.model.collection.DateOfExportedImages;
import com.softcell.imageexporter.model.collection.ExportedImagesLog;
import com.softcell.imageexporter.model.collection.gonogo.Document;

import java.util.Date;
import java.util.List;

/**
 * Created by mahesh on 19/1/18.
 */
public interface ImageExporterManager {

    /**
     * @param institutionId
     * @param referenceId
     * @return
     */
    List<Document> getDocumentsToZip(String institutionId, String referenceId);

    /**
     *
     * @param institutionId
     * @return
     */
    DateOfExportedImages getExportedApplicationDate(String institutionId);

    /**
     *
     * @param date
     * @return
     */
    List<ExportedImagesLog> getExportedImagesAgainstDate(Date date,String institutionId);
}
