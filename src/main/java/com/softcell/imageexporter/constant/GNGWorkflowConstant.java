package com.softcell.imageexporter.constant;

/**
 * @author kishor
 */
public enum GNGWorkflowConstant {

    /**
     * Surrogate type related enums.
     */
    BANK_SURROGATE("BANK_SURROGATE"),
    CAR_SURROGATE("CAR_SURROGATE"),
    HOUSE_SURROGATE("HOUSE_SURROGATE"),
    SALARIED_SURROGATE("SALARIED_SURROGATE"),
    TRADER_SURROGATE("TRADER_SURROGATE"),
    CREDIT_SURROGATE("CREDIT_SURROGATE"),
    DEBIT_CARD_SURROGATE("DEBIT_CARD_SURROGATE"),
    OLD_MOBILE_PHONE_SURROGATE("OLD_MOBILE_PHONE_SURROGATE"),
    POST_PAID_BILL_SURROGATE("POST_PAID_BILL_SURROGATE"),

    /**
     * Document image type;
     */
    BANK_STATEMENT("BANK_STATEMENT"),
    DEBIT_CARD("DEBIT_CARD"),
    PAN("PAN"),
    AADHAAR("AADHAAR"),
    DRIVING_LICENSE("DRIVING_LICENSE"),
    PASSPORT("PASSPORT"),
    VOTER_ID("VOTERID"),
    RATION_CARD("RATION-CARD"),
    APPLICANT_PHOTO("APPLICANT-PHOTO"),
    AGREEMENT_FORM("AGREEMENT_FORM"),
    INVOICE_IMAGE("INVOICE_IMAGE"),
    DELIVERY_ORDER("DELIVERY_ORDER"),
    APPLICATION_FORM("APPLICATION_FORM"),
    INCOME_PROOF1("INCOME-PROOF1"),
    INCOME_PROOF2("INCOME-PROOF2"),
    CUSTOMER_PHOTO("CUSTOMER-PHOTO"),
    AADHAAR_PHOTO("AADHAAR-PHOTO"),
    ADDITIONAL_KYC("ADDITIONAL_KYC"),
    BANK_PASSBOOK("BANK-PASSBOOK"),
    LPG_BILL("LPG-BILL"),
    ELECTRICITY_BILL("ELECTRICITY-BILL"),
    INSURANCE_APPLICATION("INSURANCE-APPLICATION"),
    ADM("ADM"),
    ECS("ECS"),
    VOTERID_PHOTO("VOTER-ID"),


    /**
     * Proof types
     */
    ID_NO_PROOF("ID_NO_PROOF"),
    DOB_PROOF_TYPE("DOB_PROOF_TYPE"),
    SIGNATURE_PROOF_TYPE("SIGNATURE_PROOF_TYPE"),
    PRESENT_ADDRESS_PROOF_TYPE("PRESENT_ADDRESS_PROOF_TYPE"),
    PERMANENT_ADDRESS_PROOF_TYPE("PERMANENT_ADDRESS_PROOF_TYPE"),


    /**
     * Decisions
     */
    APPROVED("Approved"),
    QUEUED("Queue"),
    ON_HOLD("OnHold"),
    DECLINED("Declined"),
    REPROCESSING("Reprocessing"),
    DEDUPE_QUEUE("DedupeQueue"),
    CANCELLED("Cancelled"),

    /* ACTIONS */
    LOGIN("Log in"),
    LOGOUT("Log out"),
    CHANGE_PASSWORD("Password change"),
    RESET_PASSWORD("Password reset"),
    CRO_APPROVAL("CRO approve"),
    CRO_DECLINE("CRO decline"),
    CRO_ONHOLD("CRO onhold"),
    INVOICE_GENERATION("Invoice generation"),
    LOS_STATUS_UPDATE("LOS status update"),
    COMPONENT_ACTION("Component action"),
    FILE_HANDOVER("File handover"),
    REINITIATE("Reinitiate"),
    QDE_COMPONENT("QDE Component Action"),
    SR_NR_VALIDATION("Serial number validation"),
    IMEI_VALIDATION("IMEI validation"),
    SR_NR_ROLLBACK("Serial number rollback"),
    TVR_UPDATE("TVR Update"),
    TVR_STATUS_POSITIVE("TVR Positive"),
    TVR_STATUS_NEGATIVE("TVR Negative"),
    TVR_STATUS_NOT_CONTACTABLE("TVR Not Contactable"),
    TVR_STATUS_CANCELLED("TVR Cancelled"),
    GET_DRIV_LIC_DETAIL("Get Driving License details"),
    GET_DRIV_LIC_DETAIL_V2("Get Driving License details V2"),
    GET_LPG_DETAIL("Get Lpg details"),
    GET_LPG_DETAIL_V2("Get Lpg details V2"),
    SAP_API_ENQUIRY("Sap Api Enquiry"),
    GET_ELEC_BILL_DETAIL("Get Electricity bill details"),
    GET_ELEC_BILL_DETAIL_V2("Get Electricity bill details V2"),
    GET_VOTERID_DETAIL("Get VoterId details"),
    GET_VOTERID_DETAIL_V2("Get VoterId details V2"),
    EXTERNAL_API_CALL("External API call"),
    DISBURSE_LOAN("Disbruse Loan"),
    SAVE_QDE_EXPRESS_DATA("Save QDE Express customer data"),
    SAVE_QDE_EXISTING_DATA("Save QDE Existing customer data"),
    SAVE_QDE_SOLICITED_DATA("Save QDE Solicited customer data"),
    PARTIAL_SAVE_QDE_SOLICITED_DATA("Partial Save QDE Solicited customer data "),
    PARTIAL_SAVE_QDE_EXISTING_DATA("Partial Save QDE Existing customer data"),
    PARTIAL_SAVE_QDE_EXPRESS_DATA("Partial Save QDE Express customer data"),
    RESET_APP_STATUS("Reset Application Status"),
    ACC_NR_VALIDATION("Account number validation"),

    /*  Stages     */
    DE("DE"),
    DE1("DE1"),
    DE2("DE2"),
    DE3("DE3"),
    QDE("QDE"),
    BRE("BRE"),
    CR_Q("CR_Q"),
    APRV("APRV"),
    DCLN("DCLN"),
    CR_H("CR_H"),
    POST_DECISION_DATA_ENTRY("PD_DE"),
    DO("DO"),
    SRNV("SRNV"),
    IMEIV("IMEIV"),
    INV_GNR("INV_GNR"),
    LOS_QDE("LOS_QDE"),
    LOS_APRV("LOS_APRV"),
    LOS_DCLN("LOS_DCLN"),
    LOS_DISB("LOS_DISB"),
    LOS_BDE("LOS_BDE"),
    LOS_CAN("LOS_CAN"),
    LOS_ERROR("LOS_ERROR"),
    DISB("DISB"),
    DEDUPE_FOUND("DEDUPE_FOUND"),
    CNCLD("CNCLD"),

    /*      Steps     */
    DE_STEP1("Step1"),
    DE_STEP2("Step2"),
    DE_STEP3("Step3"),
    DE_STEP4("Step4"),
    CRO_DECISION("CRO decision"),
    TVR_STATUS_UPDATE("TVR Status Update"),
    DEDUPE("Dedupe"),
    MB("MB"),
    NTC("NTC Scoring"),
    ADDRESS_VERIFICATION("Address Scoring"),
    SOBRE("SOBRE Scoring"),
    CREDIT_VIDYA_API_CALL("Credit Vidya API Call"),
    TVS_CD_API_CALL("TVS CD API Call"),

    USER_SYSTEM ( "SYSTEM"),
    /**
     * Bureaus
     */
    CIBIL("CIBIL"),
    EXPERIAN("EXPERIAN"),
    HIGHMARK("HIGHMARK"),
    EQUIFAX("EQUIFAX"),

    /**
     * Bureau Status for SOA
     */

    SUCCESS_SOA("-1"),

    /**
     * Bureau Status
     */
    BUREAU_SUCCESS("SUCCESS"),
    BUREAU_ERROR("BUREAU-ERROR"),

    /**
     * source locations
     */
    GNG_WEB("GNG_WEB"),

    /**
     * Address Types
     */
    PERMANENT("PERMANENT"),
    OFFICE("OFFICE"),
    RESIDENCE("RESIDENCE"),
    OTHER("OTHER"),
    AADHAAR_ADDRESS("AADHAAR_ADDRESS"),
    LPG_ADDRESS("LPG_ADDRESS"),
    DL_ADDRESS("DL_ADDRESS"),
    ELEC_ADDRESS("ELEC_ADDRESS"),
    VOTERID_ADDRESS("VOTERID_ADDRESS"),


    CDL("CDL"),
    DPL("DPL"),

    /**
     * Email Types
     */
    PERSONAL_EMAIL("PERSONAL_EMAIL"),
    OFFICE_EMAIL("OFFICE_EMAIL"),
    WORK("WORK"),
    PERSONAL("PERSONAL"),
    /**
     * phone type
     */
    PERSONAL_MOBILE("PERSONAL_MOBILE"),
    PERSONAL_PHONE("PERSONAL_PHONE"),
    RESIDENCE_MOBILE("RESIDENCE_MOBILE"),
    OFFICE_MOBILE("OFFICE_MOBILE"),
    RESIDENCE_PHONE("RESIDENCE_PHONE"),
    OFFICE_PHONE("OFFICE_PHONE"),

    /**
     * AuditType
     */
    RE_APPRAISE_RE_INITIATE("RE_APPRAISE_RE_INITIATE"),
    RE_INITIATE("RE_INITIATE"),

    /**
     * Gender
     */
    M("M"),
    F("F"),
    MALE("Male"),
    FEMALE("Female"),
    SINGLE("Single"),
    MR("Mr"),
    MRS("Mrs"),
    MS("Ms"),

    /**
     * Reports
     */
    DO_REPORT("DO-REPORT"),
    CIBIL_REPORT("CIBIL-REPORT"),
    PDF_REPORT("PDF-REPORT"),

    /**
     * Status
     */
    SUCCESS("SUCCESS"),
    ERROR("ERROR"),
    FAILED("FAILED"),
    NOT_AVAILABLE("N/A"),

    /**
     * Employee Type
     */
    SALARIED("Salaried"),

    /**
     * Other
     */
    DEFAULT("default"),
    AUTH_KEY("AUTHENTICATION"),
    AUTH_KEY_V2("AUTHENTICATIONV2"),
    AUTH_KEY_V3("AUTHENTICATIONV3"),

    /**
     * Third party
     */

    SAMSUNG("SAMSUNG"),
    LG("LG"),
    SONY("SONY"),
    INTEX("INTEX"),
    APPLE("APPLE"),
    GIONEE("GIONEE"),
    PANASONIC("PANASONIC"),
    VIDEOCON("VIDEOCON"),



    /**
     * Asset Categories
     */
    MOBILE("MOBILE"),


    /**
     * asset manufacturer
     */
    VANILLA("VANILLA"),

    /**
     * DMS folder type
     */
    SOURCING("SOURCING"),
    UNDERWRITING("UNDERWRITING"),
    DISBURSAL("DISBURSAL"),

    /**
     * Repayment Types
     */
    REPAYMENT_TYPE_ECS("ECS"),
    REPAYMENT_TYPE_ADM("ADM"),
    REPAYMENT_TYPE_ACH("ACH"),

    TVR_ENDUSAGE_SELF("Self"),
    TVR_ENDUSAGE_GIFT("Gift"),

    ASSET_DELIVERY_STATUS_YES("YES"),
    ASSET_DELIVERY_STATUS_NO("NO"),

    /**
     * SMS Type
     */
    DISBURSEMENT_SMS("DISBURSEMENT_SMS"),
    APPROVAL_SMS("APPROVAL_SMS"),
    OTP_SMS("OTP_SMS"),
    EXTENDED_WARRANTY_SMS("EXTENDED_WARRANTY_SMS"),
    DOWNLOAD_LINK_SMS("DOWNLOAD_LINK_SMS"),

    /**
     * Bank account Yes/No
     */
    BANK_ACCOUNT_YES("YES"),
    BANK_ACCOUNT_NO("NO"),

    PF_TYPE_P("P"),
    PF_TYPE_F("F"),
    MFR_SUBVENTION_TYPE_P("P"),
    MFR_SUBVENTION_TYPE_N("N"),
    DEALER_SUBVENTION_TYPE_P("P"),
    DEALER_SUBVENTION_TYPE_N("N"),
    /**
     * Los Status
     */
    YES("Y"),
    NO("N"),

    MATCH("Match"),
    NO_MATCH("No Match"),

    //TVS - SAP file generation.
    CD("CD"), //Portfolio

    /**
     * temporary for prosoectNumber generation
     */
    SEQUENCE_TYPE("PROSPECTNUM_SEQUENCE"),
    PDF("PDF"),
    NPCIXML("NPCIXML"),

    /**
     * Sap enquiry
     */
    SAP_ACTION("I"),
    SAP_ERROR("ER"),
    INSERT_SAP_INTERFACE("insertSapInterfaceLos"),

    /**
     * IMPS
     */
    IMPS_ATTEMPT_PREFIX("A");

    private final String stringValue;

    GNGWorkflowConstant(final String s) {
        stringValue = s;
    }

    /**
     * method to return enum constants, face values
     *
     * @return
     */
    public String toFaceValue() {
        return stringValue;
    }

    public final static String requestInitializationString = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";



}
