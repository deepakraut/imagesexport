package com.softcell.imageexporter.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by mahesh on 18/1/18.
 */
public class DateUtils {

    private static DateTimeFormatter ddMMyyyySeparatedBySlashFormat = DateTimeFormat.forPattern("dd-MM-yyyy");



    public static Date getZeroTimeDate(Date tempDate) {
        Date zeroTimeDate;
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(tempDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        zeroTimeDate = calendar.getTime();

        return zeroTimeDate;
    }

    public static Date getZeroTimeFromDate(Date fromDate) {
        Date formattedFrmDt = new DateTime(fromDate).withHourOfDay(00)
                .withMinuteOfHour(00).withSecondOfMinute(00).toDate();

        return formattedFrmDt;
    }
    public static Date getEndTimeToDate(Date toDate) {
        Date formattedToDt = new DateTime(toDate).withHourOfDay(23)
                .withMinuteOfHour(59).withSecondOfMinute(59).toDate();

        return formattedToDt;
    }

    public static Date getOneDayAfterDate(Date date) {
        Date oneDayAfterDate = null;
        if (null != date) {
            Calendar calender = Calendar.getInstance();
            calender.setTime(date);
            calender.add(Calendar.DATE, 1);
            oneDayAfterDate = calender.getTime();
        }
        return oneDayAfterDate;
    }
    public static Date getNoOfDayBeforeDate(Date date , int noOfDay) {
        Date noOfDayBeforeDate = null;
        if (null != date) {
            Calendar calender = Calendar.getInstance();
            calender.setTime(date);
            calender.add(Calendar.DATE, -noOfDay);
            noOfDayBeforeDate = calender.getTime();
        }
        return noOfDayBeforeDate;
    }

    public static String getDDMMYYYYSeperatedBySlashFormat(Date dateTime) {
        return ddMMyyyySeparatedBySlashFormat.print(dateTime.getTime());
    }

    public static boolean isDateMatch(Date otherDate) {

        Date currentDate = new Date();
        if (getZeroTimeDate(otherDate).compareTo(getZeroTimeDate(currentDate)) == 0) {
            return true;
        } else {
            return false;
        }
    }


    public static boolean checkSchedulerEndTiming() {

        //set formatted date time to 06.00.00 which is scheduler stop  time
        Date formattedDt = new DateTime(new Date()).withHourOfDay(06)
                .withMinuteOfHour(00).withSecondOfMinute(00).toDate();

        int compareFlag = new Date().compareTo(formattedDt);


        // 0 means both values are same
        // 1 means date is greater then argument date
        // -1 means date is less than the argument date
        if (compareFlag == 0 || compareFlag == 1) {
            return true;
        } else {
            return false;
        }
    }


    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date parse1=null;
        Date parse2=null;
        Date zeroTimeFromDate = getZeroTimeDate(new Date());
        Date endTimeToDate = getZeroTimeDate(new Date());

        if(zeroTimeFromDate.compareTo(endTimeToDate)==0){
            System.out.println("same");
        }else{
            System.out.println("not same");
        }


    }

    public static boolean matchToFromDate(Date startDate, Date endDate) {
        int flag = getZeroTimeDate(startDate).compareTo(getZeroTimeDate(endDate));
        System.out.println("start"+getZeroTimeDate(startDate));
        System.out.println("enddate"+getZeroTimeDate(endDate));
        System.out.println("flag"+flag);
        if (flag == 0 || flag == 1) {
            return true;
        } else {
            return false;
        }
    }
}
