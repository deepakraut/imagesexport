package com.softcell.imageexporter.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WelcomeController {

    @Value("${welcome.message}")
    private String message;

    @RequestMapping("/")
    public
    @ResponseBody
    String welcome() {
        return message;
    }


}