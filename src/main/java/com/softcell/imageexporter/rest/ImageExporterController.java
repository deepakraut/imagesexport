package com.softcell.imageexporter.rest;

import com.softcell.imageexporter.model.collection.DateOfExportedImages;
import com.softcell.imageexporter.model.collection.ExportedImagesLog;
import com.softcell.imageexporter.service.ImageExporterManager;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * Created by mahesh on 21/1/18.
 */
@RestController
@RequestMapping(
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
        headers = "accept=" + MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class ImageExporterController {

    @Autowired
    private ImageExporterManager imageExporterManager;

    private static final Logger logger = LoggerFactory.getLogger(ImageExporterController.class);


    @GetMapping("get-exported-application-date" + "/{institutionId}")
    public ResponseEntity<DateOfExportedImages> getExportedApplicationDates(
            @PathVariable("institutionId") @NotBlank @Valid String institutionId) throws Exception {

        logger.debug(" {} controller started with parameters as [{}]",
                "get-exported-images-date",
                institutionId);

        return new ResponseEntity<>(imageExporterManager.getExportedApplicationDate(institutionId), HttpStatus.OK);

    }

    @GetMapping("get-exported-images-details-against-date" + "/{date}/{institutionId}")
    public ResponseEntity<List<ExportedImagesLog>> getExportedImagesAgainstDate(
            @PathVariable("date") @DateTimeFormat(pattern = "ddMMyyyy") Date date,
            @PathVariable("institutionId") @NotBlank @Valid String institutionId) throws Exception {

        logger.debug(" {} controller started with parameters as [{},{}]",
                "get-exported-images-details-against-date",
                date, institutionId);

        System.out.println("Date"+date);

        return new ResponseEntity<>(imageExporterManager.getExportedImagesAgainstDate(date,institutionId), HttpStatus.OK);

    }
}
