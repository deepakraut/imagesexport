package com.softcell.imageexporter.scheduler;

import com.jcraft.jsch.*;
import com.softcell.imageexporter.constant.Status;
import com.softcell.imageexporter.dao.ImagesExporterRepository;
import com.softcell.imageexporter.model.collection.ExportedImagesLog;
import com.softcell.imageexporter.model.collection.WFJobCommDomain;
import com.softcell.imageexporter.model.collection.gonogo.Document;
import com.softcell.imageexporter.service.ImageExporterManager;
import com.softcell.imageexporter.utils.DateUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ResourceUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Executable;
import java.security.Security;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@EnableScheduling
@Component
public class ImageExportToSftpServerScheduler {

    private static final Logger logger = LoggerFactory.getLogger(ImageExportToSftpServerScheduler.class);

    @Value("${gonogo.hdfc.institutionId}")
    private String institutionId;

    @Value("${gonogo.hdfc.imageexport.homedir}")
    private String homeDirectory;

    @Value("${gonogo.hdfc.imageexport.toFromDateEnable}")
    private boolean toFromDateEnable;

    @Autowired
    private ImageExporterManager imageExporterManager;

    @Autowired
    private ImagesExporterRepository imagesExporterRepository;

    @Scheduled(cron = "${gonogo.hdfc.imagecron}")
    public void scheduleTaskWithCronExpression() {

        System.out.println("jod started");
        /*if (toFromDateEnable) {
            schedulerWithToAndFromDate();
            System.out.println("inside to from date of scheduler");
        } else {
            schedulerWithContineousDate();
        }*/

        pushImagesToSFTPServer();

    }

    private void pushImagesToSFTPServer() {

        String referenceId="441A00525";

        List<Document> documentsToZip = imageExporterManager.getDocumentsToZip(institutionId, referenceId);

        String path = homeDirectory;

        WFJobCommDomain wfJobCommDomain=new WFJobCommDomain();


        wfJobCommDomain.setUsername("Clentradoc");// user id
        wfJobCommDomain.setBaseUrl("efg.hdfcbank.com");// host
        wfJobCommDomain.setBasePort(22); // sftp port
        wfJobCommDomain.setPassword("LentraTeam@12345");
        wfJobCommDomain.setProxyHost("172.30.155.42");
        wfJobCommDomain.setProxyPort(3128);



        try {

            for (Document document : documentsToZip) {

                String fileName = referenceId + "_" + document.getDocName();

                upload(document,wfJobCommDomain,fileName,path);

            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public String upload(Document document, WFJobCommDomain wfJobCommDomain, String fileName,String directory) {
        String result = null;

        String privateKey="/home/dv-stl-07/Documents/Putty Keys/Latest Keys/clentradoc_privatekey.ppk";
      /*  try {
           privateKey=   ResourceUtils.getFile("classpath:clentradoc_privatekey.ppk").getAbsolutePath();
        }catch (Exception e){
            e.printStackTrace();
        }*/
        logger.warn("private key path is {}",privateKey);
        try(ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(document.getByteCode())) {

            logger.warn("SFTP Configuration {}", wfJobCommDomain);

            JSch jsch = new JSch();
            Security.insertProviderAt(new BouncyCastleProvider(), 1);

            jsch.addIdentity(privateKey);

            Session session = jsch.getSession(wfJobCommDomain.getUsername(), wfJobCommDomain.getBaseUrl(), wfJobCommDomain.getBasePort());

            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(wfJobCommDomain.getPassword());

           // ProxySOCKS4 proxy = new ProxySOCKS4(wfJobCommDomain.getProxyHost() , wfJobCommDomain.getProxyPort());
           // proxy.setUserPasswd(wfJobCommDomain.getProxyUsername(), wfJobCommDomain.getProxyPassword());
            session.setProxy(new ProxyHTTP(wfJobCommDomain.getProxyHost(), wfJobCommDomain.getProxyPort()));
            logger.warn("Proxy server with IP {} and with port {} connected successfully.", wfJobCommDomain.getProxyHost() , wfJobCommDomain.getProxyPort());

            logger.info("Trying to connect SFTP server.....");
            session.connect();
            logger.warn("Session for IP {} connected successfully", wfJobCommDomain.getBaseUrl());

            Channel channel = session.openChannel("sftp");
            channel.connect();
            logger.warn("SFTP user login with username {} successfully", wfJobCommDomain.getMemberId());

            ChannelSftp sftpChannel = (ChannelSftp) channel;
            sftpChannel.cd(directory);

            sftpChannel.put(byteArrayInputStream,fileName);
            logger.warn("FTP file with filename {} uploaded successfully", fileName);

            result = "sftp://" + wfJobCommDomain.getBaseUrl() + "/" + directory + "/" + fileName;
            logger.warn("File upload result : {}", result);

            sftpChannel.exit();
            channel.disconnect();
            session.disconnect();
            logger.warn("FTP user logout successfully");
        } catch (Exception e) {
            logger.error("Exception occurred while in ftp upload ",e);
        } finally {
            if(logger.isInfoEnabled()) {
                logger.info("Closing a byteArrayInputStream");
            }
        }
        return result;
    }


    private void schedulerWithContineousDate() {

        Date previousExportDate=null;
        Date endTimeToDate = null;
        Date zeroTimeFromDate = null;

        previousExportDate = imagesExporterRepository.getDateOfPreviousExportedImages(institutionId);

        while (null != previousExportDate) {
            try {
                Date oneDayAfterDate = DateUtils.getOneDayAfterDate(previousExportDate);

                if (null != oneDayAfterDate) {

                    zeroTimeFromDate = DateUtils.getZeroTimeFromDate(oneDayAfterDate);
                    endTimeToDate = DateUtils.getEndTimeToDate(oneDayAfterDate);

                    List<String> referenceIds = imagesExporterRepository.getApplicationIdsAgainstParticularDate(zeroTimeFromDate, endTimeToDate,institutionId);

                    logger.debug("{} reference ids found against the date for image export {}", referenceIds.size(), zeroTimeFromDate);

                    for (String referenceId : referenceIds) {


                        List<Document> documentsToZip = imageExporterManager.getDocumentsToZip(institutionId, referenceId);
                        if (!CollectionUtils.isEmpty(documentsToZip)) {

                            makeZipAndStoreInSpecificLocation(documentsToZip, referenceId, endTimeToDate);

                        } else {
                            ExportedImagesLog exportedImagesLog = new ExportedImagesLog();
                            exportedImagesLog.setApplicationDate(endTimeToDate);
                            exportedImagesLog.setExportedDate(new Date());
                            exportedImagesLog.setRefId(referenceId);
                            exportedImagesLog.setExportImageStatus(Status.FAILED.name());
                            exportedImagesLog.setMessage("Images Not found against the Application");

                            imagesExporterRepository.saveExportedImagesLog(exportedImagesLog);
                        }


                    }
                    // update exported images date in the dateOfExportedImages collection
                    imagesExporterRepository.updateDateOfExportedImages(endTimeToDate, institutionId);

                    // end the scheduler when schedular date is match with todays date or when timing is not between 11 am 6 am
                    // || DateUtils.checkSchedulerEndTiming()
                    if (DateUtils.isDateMatch(endTimeToDate)) {
                        previousExportDate = null;
                        System.out.println("inside the date math function");
                        logger.debug("scheduler is terminated due to schedular date is match with todays date or time not between 11 pm to 6 am ");
                    } else {
                        previousExportDate = endTimeToDate;
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
                previousExportDate = endTimeToDate;
            }
        }
    }

    private void makeZipAndStoreInSpecificLocation(List<Document> documentsToZip, String referenceId,Date appDate) {

        String dateFolder = DateUtils.getDDMMYYYYSeperatedBySlashFormat(appDate);
        String path = homeDirectory + "/" + dateFolder;
        File directory = new File(path);

        // check date directory path exist or not
        if (!directory.exists()) {
            directory.mkdir();
        }

        String losID = documentsToZip.get(0).getLosID();

        System.out.println("created directory ="+path);

        int successDoc =0;
        int failedDoc=0;
        List<Document> succesFailDocInfo=new ArrayList<>();
        FileOutputStream fOut = null;
        File file = null;

        try {

            for (Document document : documentsToZip) {

                try {

                    String fileName = path + "/" + referenceId +"_"+losID+"_"+ document.getDocName();

                    file = new File(fileName);
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    fOut = new FileOutputStream(fileName);

                    fOut.write(document.getByteCode());
                    fOut.flush();
                    fOut.close();
                    successDoc++;
                    document.setStatus(Status.SUCCESS.name());
                    document.setByteCode(null);
                    succesFailDocInfo.add(document);
                } catch (Exception e) {
                    e.printStackTrace();
                    failedDoc++;
                    document.setByteCode(null);
                    document.setStatus(Status.FAILED.name());
                    succesFailDocInfo.add(document);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (null != fOut) {
                try {
                    fOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // save exported images log
        if(!CollectionUtils.isEmpty(succesFailDocInfo)){
            ExportedImagesLog exportedImagesLog=new ExportedImagesLog();
            exportedImagesLog.setExportedDate(new Date());
            exportedImagesLog.setApplicationDate(appDate);
            exportedImagesLog.setTotalNoOfImage(documentsToZip.size());
            exportedImagesLog.setNoOfSuccessImages(successDoc);
            exportedImagesLog.setNoOfFailedImages(failedDoc);
            exportedImagesLog.setRefId(referenceId);
            exportedImagesLog.setExportImageStatus(Status.SUCCESS.name());
            exportedImagesLog.setSuccessFailDocInfo(succesFailDocInfo);

            imagesExporterRepository.saveExportedImagesLog(exportedImagesLog);
        }


    }


    public void schedulerWithToAndFromDate(){

        /*Date startDate = null;
        Date endDate = null;

        DateOfExportedImages schedulerToFromDate = imagesExporterRepository.getSchedulerToFromDate(institutionId);

       *//*
        if(null != schedulerToFromDate){
            startDate= schedulerToFromDate.getStartDate();
            endDate = schedulerToFromDate.getEndDate();
            System.out.println("scheduler has been stopped due to configuration not found...");
        }*//*

        int noOfPreviousDay = 0;
        if (null != schedulerToFromDate) {
            System.out.println("Configuration found successfully for image export scheduler ");
            noOfPreviousDay = schedulerToFromDate.getNoOfPreviousDay();
        }*/

        System.out.println(" started CD POLICY BRE 3 for scheme");

        imagesExporterRepository.getThreadLossApplication("CD POLICY BRE 3", "SCHEME_DETAILS_BRE", "BANKING_DETAILS_BRE");

        System.out.println(" end of CD POLICY BRE 3  for scheme");

        System.out.println(" started DPL  POLICY BRE 3  for scheme");

        imagesExporterRepository.getThreadLossApplication("DPL  POLICY BRE 3", "SCHEME_DETAILS_BRE", "BANKING_DETAILS_BRE");

        System.out.println(" end of DPL  POLICY BRE 3  for scheme");


        ///////////////////////////////////////////////////////

        System.out.println("started CD POLICY BRE 3 for asset");

        imagesExporterRepository.getThreadLossApplication("CD POLICY BRE 3", "ASSET_DETAILS_BRE", "BANKING_DETAILS_BRE");

        System.out.println("end of CD POLICY BRE 3 for asset");

        System.out.println("started DPL  POLICY BRE 3  for asset");

        imagesExporterRepository.getThreadLossApplication("DPL  POLICY BRE 3", "ASSET_DETAILS_BRE", "BANKING_DETAILS_BRE");

        System.out.println("end of DPL  POLICY BRE 3  for asset");


/////////////////////////////////////////////////////////////////////////////

        System.out.println("started DPL POLICY BRE 4_2 for banking");

        imagesExporterRepository.getThreadLossApplication("CD POLICY BRE 4_2", "BANKING_DETAILS_BRE", "SCHEME_DETAILS_BRE");

        System.out.println("end of DPL POLICY BRE 4_2 for banking");


        System.out.println("started CD POLICY BRE 4_2 for banking");

        imagesExporterRepository.getThreadLossApplication("DPL POLICY BRE 4_2", "BANKING_DETAILS_BRE", "SCHEME_DETAILS_BRE");

        System.out.println("end of CD POLICY BRE 4_2 for banking");


        //////////////////////////////////////////////////////////////////////



        System.out.println("started DPL DPL POLICY BRE 4_1 for banking");

        imagesExporterRepository.getThreadLossApplication("DPL POLICY BRE 4_1", "BANKING_DETAILS_BRE", "ASSET_DETAILS_BRE");

        System.out.println("end of DPL DPL POLICY BRE 4_1 for banking");


        System.out.println("started CD POLICY BRE 4_1 for banking");

        imagesExporterRepository.getThreadLossApplication("CD POLICY BRE 4_1", "BANKING_DETAILS_BRE", "ASSET_DETAILS_BRE");

        System.out.println("end of CD POLICY BRE 4_1 for banking");


      /*  while (noOfPreviousDay >0) {

            Date endTimeToDate = null;
            Date zeroTimeFromDate = null;

            try {
                Date previousDate = DateUtils.getNoOfDayBeforeDate(new Date(), noOfPreviousDay);

                if (null != previousDate) {

                    zeroTimeFromDate = DateUtils.getZeroTimeFromDate(previousDate);
                    endTimeToDate = DateUtils.getEndTimeToDate(previousDate);

                    List<String> referenceIds = imagesExporterRepository.getApplicationIdsAgainstParticularDate(zeroTimeFromDate, endTimeToDate,institutionId);


                    logger.debug("{} reference ids found against the date for image export {}", referenceIds.size(), zeroTimeFromDate);
                    System.out.println("Reference Ids count:"+referenceIds.size()+" Against the Date : "+zeroTimeFromDate);

                    for (String referenceId : referenceIds) {


                        List<Document> documentsToZip = imageExporterManager.getDocumentsToZip(institutionId, referenceId);
                        if (!CollectionUtils.isEmpty(documentsToZip)) {

                            makeZipAndStoreInSpecificLocation(documentsToZip, referenceId, endTimeToDate);

                        } else {
                            ExportedImagesLog exportedImagesLog = new ExportedImagesLog();
                            exportedImagesLog.setApplicationDate(endTimeToDate);
                            exportedImagesLog.setExportedDate(new Date());
                            exportedImagesLog.setRefId(referenceId);
                            exportedImagesLog.setExportImageStatus(Status.FAILED.name());
                            exportedImagesLog.setMessage("Images Not found against the Application");

                            imagesExporterRepository.saveExportedImagesLog(exportedImagesLog);
                        }


                    }
                    // update exported images date in the dateOfExportedImages collection
                    imagesExporterRepository.updateDateOfExportedImages(endTimeToDate, institutionId);


                    // DateUtils.checkSchedulerEndTiming();
                    // end the scheduler when schedular date is match with todays date or when timing is not between 11 am 6 am
                   *//* if (DateUtils.matchToFromDate(startDate,endDate)) {
                        startDate = null;
                        System.out.println("inside the date match function");
                        logger.debug("scheduler is terminated due to schedular date is match with todays date or time not between 11 pm to 6 am ");
                    } else {
                        startDate = endTimeToDate;
                    }*//*


                }
                noOfPreviousDay--;

            } catch (Exception e) {
                e.printStackTrace();
                //noOfPreviousDay--;
               // startDate = endTimeToDate;
            }
        }*/

    }


}
