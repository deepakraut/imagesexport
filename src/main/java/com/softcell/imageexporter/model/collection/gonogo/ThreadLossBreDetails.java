package com.softcell.imageexporter.model.collection.gonogo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.*;

import java.util.Date;

/**
 * Created by dv-stl-07 on 19/10/18.
 */
@org.springframework.data.mongodb.core.mapping.Document(collection = "threadLossBreDetails")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ThreadLossBreDetails {

    private String oldRefId;

    private String oldBreType;

    private String newRefId;

    private String newBreType;

    private Date dateTime;
}
