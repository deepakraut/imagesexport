package com.softcell.imageexporter.model.collection.gonogo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Document {
    @JsonProperty("sDocID")
    private String docID;
    @JsonProperty("sStat")
    private String status;
    @JsonProperty("sDocName")
    private String docName;
    @JsonProperty("sByteCode")
    private byte [] byteCode;

    private String losID;



    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public byte[] getByteCode() {
        return byteCode;
    }

    public void setByteCode(byte[] byteCode) {
        this.byteCode = byteCode;
    }

    public String getDocID() {
        return docID;
    }

    public void setDocID(String docID) {
        this.docID = docID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getLosID() {
        return losID;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Document{");
        sb.append("docID='").append(docID).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", docName='").append(docName).append('\'');
        sb.append(", byteCode=").append(Arrays.toString(byteCode));
        sb.append(", losID='").append(losID).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public void setLosID(String losID) {
        this.losID = losID;
    }

}
