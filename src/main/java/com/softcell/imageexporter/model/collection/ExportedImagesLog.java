package com.softcell.imageexporter.model.collection;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

/**
 * Created by mahesh on 17/1/18.
 */
@Document(collection = "exportedImageLog")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExportedImagesLog {

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("iTotalNoOfImages")
    private int totalNoOfImage;

    @JsonProperty("iNoOfSuccessImg")
    private int noOfSuccessImages;

    @JsonProperty("iNoOfFailedImg")
    private int noOfFailedImages;

    @JsonProperty("sExportImageStatus")
    private String exportImageStatus;

    @JsonProperty("aSuccessFailDocInfo")
    private List<com.softcell.imageexporter.model.collection.gonogo.Document> successFailDocInfo;

    @JsonProperty("sMessage")
    private String message;

    @JsonProperty("dtAppDate")
    private Date applicationDate;

    @JsonProperty("dtExportedDate")
    private Date exportedDate;


}
