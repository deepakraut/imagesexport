package com.softcell.imageexporter.model.collection;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

/**
 * Created by mahesh on 17/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "dateOfExportedImages")

public class DateOfExportedImages {

    @JsonProperty("sDtStartDate")
    private Date startDate;

    @JsonProperty("sEndDate")
    private Date endDate;

    @JsonProperty("iNoOfPreviousDay")
    private int noOfPreviousDay;

    @JsonProperty("aDtExportedImagesDate")
    private List<Date> listOfExportedImagesDate;

    @JsonProperty("bActive")
    private boolean active =true;

    @JsonProperty("sInstitutionId")
    private String institutionId;


}
