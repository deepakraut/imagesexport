/**
 * yogeshb2:25:55 pm  Copyright Softcell Technolgy
 **/
package com.softcell.imageexporter.model.collection.gonogo;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author yogeshb
 */
public class LOSDetails {

    @JsonProperty("sLosID")
    private String losID;

    @JsonProperty("sStat")
    private String status;

    @JsonProperty("sUtr")
    private String utrNumber;

    @JsonProperty("sError")
    private String error;

    @JsonProperty("sDealerTieUpFlag")
    private String dealerTieUpFlag;

    @JsonProperty("sDealerRefNumber")
    private String dealerReferenceNumber;


    public String getLosID() {
        return losID;
    }

    public void setLosID(String losID) {
        this.losID = losID;
    }

    public String getUtrNumber() {
        return utrNumber;
    }

    public void setUtrNumber(String utrNumber) {
        this.utrNumber = utrNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getDealerTieUpFlag() {
        return dealerTieUpFlag;
    }

    public void setDealerTieUpFlag(String dealerTieUpFlag) {
        this.dealerTieUpFlag = dealerTieUpFlag;
    }

    public String getDealerReferenceNumber() {
        return dealerReferenceNumber;
    }

    public void setDealerReferenceNumber(String dealerReferenceNumber) {
        this.dealerReferenceNumber = dealerReferenceNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LOSDetails{");
        sb.append("losID='").append(losID).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", utrNumber='").append(utrNumber).append('\'');
        sb.append(", error='").append(error).append('\'');
        sb.append(", dealerTieUpFlag='").append(dealerTieUpFlag).append('\'');
        sb.append(", dealerReferenceNumber='").append(dealerReferenceNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LOSDetails that = (LOSDetails) o;

        if (losID != null ? !losID.equals(that.losID) : that.losID != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (utrNumber != null ? !utrNumber.equals(that.utrNumber) : that.utrNumber != null) return false;
        if (error != null ? !error.equals(that.error) : that.error != null) return false;
        if (dealerTieUpFlag != null ? !dealerTieUpFlag.equals(that.dealerTieUpFlag) : that.dealerTieUpFlag != null)
            return false;
        return dealerReferenceNumber != null ? dealerReferenceNumber.equals(that.dealerReferenceNumber) : that.dealerReferenceNumber == null;
    }

    @Override
    public int hashCode() {
        int result = losID != null ? losID.hashCode() : 0;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (utrNumber != null ? utrNumber.hashCode() : 0);
        result = 31 * result + (error != null ? error.hashCode() : 0);
        result = 31 * result + (dealerTieUpFlag != null ? dealerTieUpFlag.hashCode() : 0);
        result = 31 * result + (dealerReferenceNumber != null ? dealerReferenceNumber.hashCode() : 0);
        return result;
    }
}
