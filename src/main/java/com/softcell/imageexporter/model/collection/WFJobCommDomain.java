package com.softcell.imageexporter.model.collection;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Document(collection = "wf_communication_settings")
@Data
public class WFJobCommDomain {

    @Id
    private String id;

    @NotNull
    @Field("institutionId")
    private String institutionId;

    @NotNull
    @Field("productName")
    private String productName;

    @Field("serviceName")
    private String serviceName;


    @Field("aggregatorId")
    private String aggregatorId;

    @Field("memberId")
    private String memberId;

    @Field("password")
    private String password;

    @Field("licenseKey")
    private String licenseKey;

    @NotNull
    @Field("baseUrl")
    private String baseUrl;

    @NotNull
    @Field("endpoint")
    private String endpoint;

    @NotNull
    @Field("noOfRetry")
    private Integer noOfRetry;

    @Field("blockSpan")
    private Long blockSpan;

    @Field("serviceId")
    private String serviceId;

    @Field("isEnabled")
    private Boolean isEnabled;

    @Field("updatedDt")
    private Date updatedDt;

    @Field("insertDt")
    private Date insertDt;

    @NotNull
    @Field("type")
    private String type;


    @Field("helpDeskNumber")
    private String helpDeskNumber;

    @Field("sessionType")
    private String sessionType;

    @Field("dedupeCheck")
    private boolean dedupeCheck;

    @Field("applicationId")
    private String applicationId;

    @Field("geoTaggingDistanceLimit")
    private String geoTaggingDistanceLimit;

    @Field("masterNames")
    private List masterNames;

    @Field("channelId")
    private String channelId;

    @Field("accessId")
    private String accessId;

    @Field("accessCode")
    private String accessCode;

    @Field("sourceSystem")
    private String sourceSystem;

    @Field("institutionName")
    private String institutionName;

    @Field("grant_type")
    private String grant_type;
    @Field("username")
    private String username;
    @Field("clientName")
    private String clientName;
    @Field("clientPASS")
    private String clientPASS;

    @Field("authBaseUrl")
    private String authBaseUrl;

    @Field("authEndPoint")
    private String authEndPoint;

    @Field("proxyHost")
    private String proxyHost;

    @Field("proxyPort")
    private int proxyPort;

    @Field("proxyUsername")
    private String proxyUsername;

    @Field("proxyPassword")
    private String proxyPassword;
    @Field("retryInterval")
    private long retryInterval;

    @Field("callbackUrl")
    private String callbackUrl;


    @Field("enviroment")
    private String enviroment;

    @Field("serviceDown")
    private boolean serviceDown;

    @Field("creditorAgentCode")
    private String creditorAgentCode;

    @Field("creditorUtilityCode")
    private String creditorUtilityCode;

    @Field("basePort")
    private int basePort;

    @Field("mode")
    private String mode;

    @Field("batchSize")
    private int batchSize;

    @Field("transIdLength")
    private int transIdLength;

    @NotNull
    @Field("nameMatchingValue")
    private Integer nameMatchingValue;

    @Field("waitInMillis")
    private int waitInMillis;

    @Field("userEmail")
    private String userEmail;

    @Field("allowedChar")
    private String allowedChar;


    @Field("userRole")
    private String userRole;

}
